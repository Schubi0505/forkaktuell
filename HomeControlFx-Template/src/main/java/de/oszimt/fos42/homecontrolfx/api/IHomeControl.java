package de.oszimt.fos42.homecontrolfx.api;

/**
 * Interface to the HomeControlFX Server. Offers authentication methods and access to light,
 * brightness and temperature. 
 * 
 * @author M. Schleyer (schleyer@oszimt.de)
 *
 */
public interface IHomeControl {

	// Auth
	/**
	 * Sets the AUTH token for the application to be used from now on. 
	 * <p>
	 * the key is not checked for validity, use <code>checkAuthKey</code> to do so
	 * 
	 * @param key		AUTH token to be used from now on
	 */
	public void setAuthKey(String key);
	
	/**
	 * Generates an AUTH token using an user name and a password. This token
	 * can be passed to <code>setAuthKey</code>.
	 * <p>
	 * Please note that the generated token is not set to the interface, this needs to be
	 * done explicitly with <code>setAuthKey</code>
	 * 
	 * @param username	user name for the new AUTH token
	 * @param password  password for the new AUTH token
	 * @return  		the newly generated AUTH token
	 * @see     		setAuthKey
	 */
	public String generateAuthKey(String username, String password);
	
	/**
	 * checks if the current AUTH token is accepted by the server
	 * 
	 * @return  <code>true</code> if the AUTH token was accepted, <code>false</code> otherwise
	 * @see     setAuthKey
	 */
	public boolean checkAuthKey();

	// Light control
	/**
	 * set the light state to the given value
	 * 
	 * @param state  state of the light as boolean expression
	 * @see   getLightState
	 */
	public void setLightState(boolean state);
	
	/**
	 * reads the current light state from the server
	 * 
	 * @return the light state (<code>true</code> = ON, <code>false</code> = OFF)
	 */
	public boolean getLightState();
	
	/**
	 * switches the light state (either ON to OFF or OFF to ON)
	 * 
	 * @return state of the light 
	 * @see    getLightState()
	 */
	public boolean switchLightState();

	// Temperature control
	/**
	 * returns the current temperature value
	 * 
	 * @return current temperature in degree Celsius
	 */
	public double getTemperature();

	/**
	 * gets the user login as currently used to login to the server
	 * 
	 * @return the user login as obtained from the server
	 */
	public String getUserLogin();

	/**
	 * gets the full user name of the user
	 * 
	 * @return the full login name as obtained from the server
	 */
	public String getUserName();

}
