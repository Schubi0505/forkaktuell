package de.oszimt.fos42.homecontrolfx.console;

import java.util.Scanner;

import de.oszimt.fos42.homecontrolfx.api.HomeControlSim;
import de.oszimt.fos42.homecontrolfx.api.IHomeControl;

public class HomeControlConsole {

	IHomeControl myControl;

	public HomeControlConsole(IHomeControl _myControl) {
		this.myControl = _myControl;
		mainLoop();
	}

	public void mainLoop() {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		while (true) {
			// Login
			System.out.println("*************************************");
			System.out.println("* Willkommen bei HomeControlConsole *");
			System.out.println("*************************************");
			boolean loginOk = false;
			while(true) {
				System.out.print("Benutzer: ");
				String username = in.nextLine();
				System.out.print("Passwort: ");
				String password = in.nextLine();
				myControl.setAuthKey(myControl.generateAuthKey(username, password));
				loginOk = myControl.checkAuthKey();
				if (loginOk)
					break;
				System.out.println("Benutzername oder Passwort nicht bekannt!");
			};
			// Main Menu
			boolean runLoop = true;
			while (runLoop)
			{
				System.out.println("*************************************");
				System.out.format(" Temperatur: %03.2f °C%n", myControl.getTemperature());
				System.out.format(" Licht:      %s%n", (myControl.getLightState() ? "an" : "aus"));
				System.out.println("*************************************");
				System.out.println(" 1.) Licht umschalten");
				System.out.println("*************************************");
				System.out.println(" 8.) Aktualisieren");
				System.out.println(" 9.) Beenden");
				System.out.println("*************************************");
				System.out.print("> ");
				int yourChoice = in.nextInt();
				switch (yourChoice)
				{
				case 1:
					myControl.switchLightState();
					break;
				case 8:
					continue;
				case 9:
					runLoop = false;
					break;
					default:
						System.out.println("Eingabe fehlerhaft!");
						break;
					
				}
			}
		}
	}

	public static void main(String[] args) {
		new HomeControlConsole(new HomeControlSim());
	}

}
