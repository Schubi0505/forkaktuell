package de.oszimt.fos42.homecontrolfx.gui;

import de.oszimt.fos42.homecontrolfx.api.UserLogin;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * <p>
 * Vorlage für den Controller. In dieser Klasse werden alle Events definiert.
 * Events sind die Aktionen, die vom Benutzer ausgelöst werden können. Dazu
 * gehört zum Beispiel der Klick auf einen Button.
 * </p>
 * 
 * @author Martin Schleyer
 *
 */
public class LoginWindowController {

	/**
	 * Erstellt ein Objekt der UserLogin-Klasse mit Test-Benutzerdaten.
	 * 
	 * @see de.oszimt.fos42.homecontrolfx.api.UserLogin
	 */
	@SuppressWarnings("unused")
	private UserLogin userLogin = new UserLogin("Hallo", "Welt!");

	/**
	 * Hier kommen die weiteren Attribute des Controllers hin. Sie können sich
	 * ein Beispiel dazu direkt im SceneBuilder anschauen.
	 * 
	 * Wählen Sie dazu "View" &gt;&gt; "Show Sample Controller Skeleton" im Menü
	 * aus, während Sie die Datei <i>LoginWindow.fxml</i> bearbeiten.
	 */

	// Attribute...
	
	@FXML
    private Label lblBenutzername;

    @FXML
    private TextField tfdBenutzername;

    @FXML
    private Button btnAnmelden;

    @FXML
    private PasswordField pfdPasswort;

    @FXML
    private Label lblPasswort;

    @FXML
    private Button btnAbbrechen;

	/**
	 * Anschließend folgen die einzelnen Events, die Sie im SceneBuilder
	 * definiert haben. Beispiel:
	 * 
	 *  protected void btnHalloWeltClick(){
	 *       System.out.println("Hallo Welt!"); }
	 */

	// Methoden ...
    
    @FXML
    protected void btnAnmeldenClick(){
    	
    String username = tfdBenutzername.getText();
    String pwd = pfdPasswort.getText();
    
    if (userLogin.checkLogin(username, pwd)){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information Dialog");
		alert.setHeaderText("Look, an Information Dialog");
		alert.setContentText("Login successful!");

		alert.showAndWait();
		
	}
	else{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information Dialog");
		alert.setHeaderText("Look, a Warning Dialog");
		alert.setContentText("Login failed!");

		alert.showAndWait();
		
	}
    	}
   @FXML
   protected void btnAbbrechenClick(){
	   
	Stage stage = (Stage) btnAbbrechen.getScene().getWindow();
	    stage.close();
   }
}
